<?php
	
	// Set up
	add_theme_support('menus');


	// Includes
	include(get_template_directory() .'/includes/front/enqueue.php');
	include(get_template_directory() .'/includes/setup.php');
	include(get_template_directory() .'/includes/widgets.php');


	// Action and filter hook
	add_action('wp_enqueue_scripts', 'ju_enqueue');
	add_action('after_setup_theme', 'ju_setup_after');
	add_action('widgets_init', 'ju_widgets');


	// Shorts codes

?>
