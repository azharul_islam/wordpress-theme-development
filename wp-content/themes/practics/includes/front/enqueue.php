<?php

	function ju_enqueue() {
		
		wp_register_style('ju_bootstrap', get_template_directory_uri().'/assets/vendor/bootstrap/css/bootstrap.css');
		wp_register_style('ju_bootstrap_min', get_template_directory_uri().'/assets/vendor/bootstrap/css/bootstrap.min.css');
		wp_register_style('ju_css', get_template_directory_uri().'/assets/css/clean-blog.css');
		wp_register_style('ju_css_min', get_template_directory_uri().'/assets/css/clean-blog.min.css');
		wp_register_style('ju_fonts', get_template_directory_uri().'/assets/vendor/font-awesome/css/font-awesome.min.css');
		wp_register_style('ju_lora', 'https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic');
		wp_register_style('ju_open_sans', 'https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800');
		
		wp_enqueue_style('ju_bootstrap');
		wp_enqueue_style('ju_bootstrap_min');
		wp_enqueue_style('ju_css');
		wp_enqueue_style('ju_css_min');
		wp_enqueue_style('ju_fonts');
		wp_enqueue_style('ju_lora');
		wp_enqueue_style('ju_open_sans');

		wp_register_script('ju_jquery', get_template_directory_uri().'/assets/vendor/jquery/jquery.min.js', array(), false, true);
		wp_register_script('ju_boot_js', get_template_directory_uri().'/assets/vendor/bootstrap/js/bootstrap.min.js', array(), false, true);
		wp_register_script('ju_valid', get_template_directory_uri().'/assets/js/jqBootstrapValidation.js', array(), false, true);
		wp_register_script('ju_contact', get_template_directory_uri().'/assets/js/contact_me.js', array(), false, true);
		wp_register_script('ju_js', get_template_directory_uri().'/assets/js/clean-blog.min.js', array(), false, true);

		wp_enqueue_style('ju_jquery');
		wp_enqueue_style('ju_boot_js');
		wp_enqueue_style('ju_valid');
		wp_enqueue_style('ju_contact');
		wp_enqueue_style('ju_js');
	}

?>