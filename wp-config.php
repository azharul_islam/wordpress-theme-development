<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WRtdq&@~%^8-/b[a8/aZ{m;ln[gK&BIz%Z2f [wL559>LgS)?&6=h4LrJR`=[X6F');
define('SECURE_AUTH_KEY',  '(q/LXlJNe#EHK9lWfl|<eJNY ~:cXnv5D:S6rND,wqq;=BTQZEd;~$}dr/p2Aj40');
define('LOGGED_IN_KEY',    'S&ulpTMqLcfr3bn^oEfC%Xc^(EUeu{,q7vOet-il!z4+%PWE>@qMWy_rQ2p6Jo:p');
define('NONCE_KEY',        'lxAS&FZex%!B48nrNDbx%?+X+B #p`OS(lLUJ=5AIA#NjsR6}c3~EJp8[:2Mr^Pb');
define('AUTH_SALT',        'r7,+hsEgHVJ2]7P1hHeo<#C8=DOUUlRImUx`6^BR?x_cVg%^+b^w;QJ%CNVQR{VQ');
define('SECURE_AUTH_SALT', '|#?7#[%=Otbi5)ZHTabo`f-k^B,..c629~8<%Jrz2%4psa;vg$1))R:w,8?1-zAl');
define('LOGGED_IN_SALT',   '5q]=rlIGGc#V:?.xDBMD@rFbkk)De{K!k^6 y/A6zsRrrA+]kDkJ^3)roK^h)azs');
define('NONCE_SALT',       '?vzm?%9wG]}Ns|p#q0=L0m?V08)QjET3kV{}.iy;4l>N7rZ0DKb &H<Am*QyJpJ2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_table';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
